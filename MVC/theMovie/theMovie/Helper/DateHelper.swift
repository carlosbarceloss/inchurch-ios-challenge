//
//  String+GetFormattedDate.swift
//  theMovie
//
//  Created by Carlos Barcelos on 05/07/21.
//

import Foundation
class DateHelper {
    static func getFormattedDate(from string: String?) -> String {
        guard let string = string else { return "Data de lançamento indisponível"}
        let dateConverter = DateFormatter()
        var formattedDate: String?
        dateConverter.dateFormat = "yyyy-MM-dd"
        if let oldDate = dateConverter.date(from: string) {
            dateConverter.dateFormat = "dd/MM/YYYY"
            formattedDate = dateConverter.string(from: oldDate)
        }
        return formattedDate!
    }
}

