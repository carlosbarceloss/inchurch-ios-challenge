//
//  MoviesListHelper.swift
//  theMovie
//
//  Created by Carlos Barcelos on 03/07/21.
//

import Foundation

protocol MoviesListHelperDelegate: AnyObject {
    
    func addMovies(_ moviesArr: [Movie])
    func getMovies() -> [Movie]
    func getMovieById(_ movieId: Int) -> Movie?
    func filterMovies(with keywords: String) -> [Movie]
    
    func addFavorite(_ movieId: Int)
    func removeFavorite(_ movieId: Int)
    func isFavorite(_ movieId: Int?) -> Bool
    func getFavoriteMovies() -> [Movie]
    
    func addGenres(_ genres: [Genre])
    func getGenresForMovie(_ movieId: Int) -> String    
}

class MoviesListHelper {
    
    private var moviesList = [Movie]()     
    private var filteredMovies = [Movie]()
    private var genresArray = [Genre]()
    
    private func getGenresString(for movie: Movie, from genresDictionary: [Genre]) -> String {
        guard let genresIdArray = movie.genre_ids else { return "Genres not specified" }
        var dictionary = [Genre]()
        for genre in genresDictionary {
            for id in genresIdArray {
                if id == genre.id {
                    dictionary.append(genre)
                }
            }
        }
        return genresDictionaryToString(dictionary)
    }
    
    private func genresDictionaryToString(_ genresDictionary: [Genre]) -> String {
        let genresArray = genresDictionary.map({return $0.name})
        let joined = ListFormatter.localizedString(byJoining: genresArray)
        return joined
    }
    
}

extension MoviesListHelper: MoviesListHelperDelegate {
    
    func addMovies(_ moviesArr: [Movie]) {
        for var movie in moviesArr {
            movie.toggleFavorite()
            moviesList.append(movie)
        }
    }
    
    func getMovies() -> [Movie] {
        return moviesList
    }
    
    func getMovieById(_ movieId: Int) -> Movie? {
        if let movie = moviesList.first(where: {$0.id == movieId}) {
            return movie
        }
        return nil
    }
    
    func getFavoriteMovies() -> [Movie] {
        return moviesList.filter({$0.isFavorite == true})
    }
    
    func filterMovies(with keywords: String) -> [Movie] {
        filteredMovies.removeAll(keepingCapacity: true)
        filteredMovies = moviesList.filter({ (movie: Movie) in
            guard let movieTitle = movie.title else { return false }
            return movieTitle.lowercased().contains(keywords.lowercased())
        })
        return filteredMovies
    }
    
    func addGenres(_ genres: [Genre]) {
        self.genresArray = genres
    }
    
    func getGenresForMovie(_ movieId: Int) -> String {
        if let movie = moviesList.first(where: {$0.id == movieId}) {
            return getGenresString(for: movie, from: genresArray)
        }
        return "Genres not found.."
    }
    
    func addFavorite(_ movieId: Int) {
        if let index = moviesList.firstIndex(where: {$0.id == movieId}) {
            moviesList[index].toggleFavorite()
        }
    }
    
    func removeFavorite(_ movieId: Int) {
        if let index = moviesList.firstIndex(where: {$0.id == movieId}) {
            moviesList[index].toggleFavorite()
        }
    }
    
    func isFavorite(_ id: Int?) -> Bool {
        guard let id = id else { return false }
        if let index = moviesList.firstIndex(where: {$0.id == id}) {
            if let isFav = moviesList[index].isFavorite {
                return isFav
            }
        }
        return false
    }
}
