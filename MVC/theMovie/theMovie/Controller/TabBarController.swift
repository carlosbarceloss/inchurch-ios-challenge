//
//  TabBarController.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import UIKit

class TabBarController: UITabBarController {
    
    private var moviesHelper = MoviesListHelper()
    private let moviesList = MoviesViewController()
    private let favoritesList = FavoritesViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().barTintColor = UIColor(red: 0.00, green: 0.04, blue: 0.07, alpha: 1.00)
        UITabBar.appearance().tintColor = .white
        setupVCs()
    }
    
    fileprivate func createNavController(for rootViewController: UIViewController,
                                                     title: String,
                                                     image: UIImage?,
                                                     selectedImage: UIImage?,
                                                     tag: Int) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        navController.tabBarItem.selectedImage = selectedImage
        navController.tabBarItem.tag = tag
        navController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navController.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navController.navigationBar.barTintColor = UIColor(red: 0.00, green: 0.04, blue: 0.07, alpha: 1.00)
        navController.navigationBar.prefersLargeTitles = true
        rootViewController.navigationItem.title = title
        return navController
       }
    
    func setupVCs() {
       viewControllers = [
        createNavController(for: moviesList, title: "Movies", image: UIImage(systemName: "film"), selectedImage: UIImage(systemName: "film.fill"), tag: 1),
               createNavController(for: favoritesList, title: "Favorites", image: UIImage(systemName: "star"), selectedImage: UIImage(systemName: "star.fill"), tag: 2)
       ]
        moviesList.helper = moviesHelper
        favoritesList.helper = moviesHelper
    }
}
