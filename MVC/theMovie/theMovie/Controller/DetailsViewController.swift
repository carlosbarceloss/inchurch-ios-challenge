//
//  DetailsViewController.swift
//  theMovie
//
//  Created by Carlos Barcelos on 05/07/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    private let screen = DetailsScene()
    weak var delegate: MoviesListHelperDelegate?
    
    override func loadView() {
        view = screen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }    
    
    func setupScreenFor(_ movie: Movie, genres: String) {
        title = movie.title
        screen.delegate = delegate
        screen.setupScreenFor(movie, genres: genres, isFavorite: delegate?.isFavorite(movie.id))
    }
    
}
