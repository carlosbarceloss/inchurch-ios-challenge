//
//  FavoritesViewController.swift
//  theMovie
//
//  Created by Carlos Barcelos on 03/07/21.
//

import UIKit

class FavoritesViewController: UITableViewController {
    
    weak var helper: MoviesListHelperDelegate?
    var favoriteMovies = [Movie]() {
        didSet {
            if favoriteMovies.isEmpty {
                noFavoritesYet.isHidden = false
            } else {
                noFavoritesYet.isHidden = true
            }
        }
    }
    
    lazy var noFavoritesYet: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "no_favorites")
        iv.contentMode = .scaleAspectFit
        iv.isHidden = true
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .background
        tableView.register(FavoritesCell.self, forCellReuseIdentifier: "Favorite")
        tableView.separatorStyle = .none
        tableView.backgroundView?.contentMode = .scaleAspectFit
        tableView.backgroundView = noFavoritesYet
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let favorites = helper?.getFavoriteMovies() else { return }
        favoriteMovies = favorites
        tableView.reloadData()
    }
    
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Remove") { [weak self ](action, view, completion) in
            guard let id = self?.favoriteMovies[indexPath.row].id else { return }
            self?.helper?.removeFavorite(id)
            self?.favoriteMovies.remove(at: indexPath.row)
            self?.tableView?.deleteRows(at: [indexPath], with: .automatic)
            
        }
        action.image = UIImage(systemName: "trash")
        action.backgroundColor = .red
        return action
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = favoriteMovies[indexPath.row]
        
        guard let movieId = movie.id else { return }
        guard let genres = helper?.getGenresForMovie(movieId) else { return }
        
        let vc = DetailsViewController()
        vc.delegate = helper
        vc.setupScreenFor(movie, genres: genres)
        navigationController?.show(vc, sender: self)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteMovies.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Favorite") as? FavoritesCell else {
            fatalError("Não foi possível encontrar a célula FavoritesCell.")
        }
        cell.setupCell(for: favoriteMovies[indexPath.row])
        cell.delegate = helper
        return cell
    }
    
}
