//
//  ViewController.swift
//  theMovie
//
//  Created by Carlos Barcelos on 03/07/21.
//

import UIKit

class MoviesViewController: UIViewController  {
    
    weak var helper: MoviesListHelperDelegate?
    private var moviesList = [Movie]()
    private var service = Service()
    private var filteredMovies = [Movie]() {
        didSet {
            if filteredMovies.isEmpty {
                noMoviesFound.isHidden = false
            } else {
                noMoviesFound.isHidden = true
            }
        }
    }
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    private var isFiltering: Bool {
        return !isSearchBarEmpty && searchController.isActive
    }
    
    let flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        cv.register(MovieCell.self, forCellWithReuseIdentifier: "Movie")
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .background
        view.addSubview(cv)
        return cv
    }()
    
    let searchController: UISearchController = {
        let sc = UISearchController()
        sc.obscuresBackgroundDuringPresentation = false
        sc.searchBar.placeholder = "Search movies.."
        sc.searchBar.tintColor = UIColor.lightText
        sc.searchBar.barStyle = .black
        return sc
    }()
    
    lazy var noMoviesFound: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "no_movies")
        iv.contentMode = .scaleAspectFit
        iv.isHidden = true
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        collectionView.delegate = self
        collectionView.backgroundView?.contentMode = .scaleAspectFit
        collectionView.backgroundView = noMoviesFound
        requestGenres()
        loadRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadMovies()
    }
    
    func loadRequest(pagination: Bool = false) {
        guard !service.isPaginating else { return }
        service.request(endpoint: ApiEndpoints.moviesList, pagination: pagination) { (result: Result<MoviesList, RequestError>) in
            switch result {
            case .success(let moviesList):
                DispatchQueue.main.async { [weak self] in
                    self?.helper?.addMovies(moviesList.results)
                    self?.moviesList = self?.helper?.getMovies() ?? []
                    self?.collectionView.reloadData()
                }
            default: break
            }
        }
    }
    
    func requestGenres() {
        service.request(endpoint: ApiEndpoints.genres) { (result: Result<Genres, RequestError>) in
            switch result {
            case .success(let genres):
                DispatchQueue.main.async { [weak self] in
                    self?.helper?.addGenres(genres.genres)
                }
            case .failure:
                break
            }
        }
    }
    
    func reloadMovies() {
        guard let helper = helper else { return }
        moviesList = helper.getMovies()
        collectionView.reloadData()
    }
    
}

extension MoviesViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let keywords = searchController.searchBar.text else { return }
        if !keywords.isEmpty {
            guard let newMovies = helper?.filterMovies(with: keywords) else { return }
            filteredMovies = newMovies
            collectionView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        noMoviesFound.isHidden = true
        reloadMovies()
    }
}

extension MoviesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if position > (collectionView.contentSize.height - 50 - scrollView.frame.size.height) {
            loadRequest(pagination: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = moviesList[indexPath.row]
        
        guard let movieId = movie.id else { return }
        guard let genres = helper?.getGenresForMovie(movieId) else { return }
        
        let vc = DetailsViewController()
        vc.delegate = helper
        vc.setupScreenFor(movie, genres: genres)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width / 2) - 16
        let height = width * 1.5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering {
            return filteredMovies.count
        }
        return moviesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Movie", for: indexPath) as? MovieCell else {
            fatalError("Não foi possível encontrar a célula MovieCell.")
        }
        cell.delegate = helper
        if isFiltering {
            cell.setupCell(for: filteredMovies[indexPath.row])
        } else {
            cell.setupCell(for: moviesList[indexPath.row])
        }
        return cell
    }
}
