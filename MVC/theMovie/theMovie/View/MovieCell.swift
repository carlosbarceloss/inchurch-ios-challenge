//
//  MovieCell.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import UIKit

class MovieCell: UICollectionViewCell, UICollectionViewDelegate {
    weak var delegate: MoviesListHelperDelegate?
    private var movieId: Int!
    
    private let moviePoster: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    private let movieTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let favButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(systemName: "star"), for: .normal)
        btn.tintColor = UIColor(red: 0.83, green: 0.69, blue: 0.22, alpha: 1.00)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private let vStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fill
        sv.alignment = .center
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    private let hStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.alignment = .center
        sv.distribution = .equalSpacing
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        favButton.addTarget(self, action: #selector(favButtonTapped(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(for movie: Movie) {
        guard let id = movie.id else { return }
        movieId = id
        movieTitle.text = movie.title
        if let posterPath = movie.poster_path {
            guard let url = Service.constructPosterUrl(from: posterPath) else { return }
            moviePoster.downloadImage(from: url)
        } else {
            moviePoster.image = UIImage(named: "poster_default")
        }
        initFavorite()
        
    }
    
    @objc func favButtonTapped(_ sender: UIButton) {
        guard let delegate = delegate else { return }
        if delegate.isFavorite(movieId) {
            delegate.removeFavorite(movieId)
            favButton.setImage(UIImage(systemName: "star"), for: .normal)
        } else {
            delegate.addFavorite(movieId)
            favButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        }
    }
    
    func initFavorite() {
        guard let delegate = delegate else { return }
        if delegate.isFavorite(movieId) {
            favButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        } else {
            favButton.setImage(UIImage(systemName: "star"), for: .normal)
        }
    }
    
}

extension MovieCell: ViewCode {
    func createViewsHierarchy() {
        addSubview(vStack)
        vStack.addArrangedSubview(moviePoster)
        vStack.addArrangedSubview(hStack)
        hStack.addArrangedSubview(movieTitle)
        hStack.addArrangedSubview(favButton)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            vStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            vStack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            vStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            vStack.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor),
            
            moviePoster.widthAnchor.constraint(equalTo: vStack.widthAnchor),
            moviePoster.heightAnchor.constraint(equalTo: vStack.heightAnchor, multiplier: 0.8),
            
            hStack.widthAnchor.constraint(equalTo: vStack.widthAnchor, multiplier: 0.8),
            
            movieTitle.heightAnchor.constraint(equalTo: hStack.heightAnchor, multiplier: 0.8)
        ])
    }
    
    func setAdditionalConfigurations() {
        vStack.backgroundColor = .cell
        movieTitle.textColor = .white
    }
}
