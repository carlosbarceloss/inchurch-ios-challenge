//
//  DetailsScene.swift
//  theMovie
//
//  Created by Carlos Barcelos on 05/07/21.
//

import UIKit

class DetailsScene: UIView {
    
    weak var delegate: MoviesListHelperDelegate?
    private var movieId: Int?
    
    private var releaseDateString: String = "" {
        didSet {
            releaseDateLabel.text = "Release date: \(releaseDateString)"
        }
    }
    
    private var isFavoriteMovie: Bool! {
        didSet {
            if isFavoriteMovie! {
                favButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
            } else {
                favButton.setImage(UIImage(systemName: "star"), for: .normal)
            }
        }
    }
    
    private let moviePoster: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    private let hStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalSpacing
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    private let releaseDateLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .lightText
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let favButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(systemName: "star"), for: .normal)
        btn.tintColor = UIColor(red: 0.83, green: 0.69, blue: 0.22, alpha: 1.00)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private let genresLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightText
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let movieDescription: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 19)
        lbl.numberOfLines = 0
        lbl.textColor = .white
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        favButton.addTarget(self, action: #selector(favButtonTapped(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupScreenFor(_ movie: Movie, genres: String, isFavorite: Bool? = false) {
        if let posterPath = movie.backdrop_path {
            guard let url = Service.constructPosterUrl(from: posterPath) else { return }
            moviePoster.downloadImage(from: url)
        } else {
            moviePoster.image = UIImage(named: "backdrop_poster_default")
        }
        isFavoriteMovie = isFavorite
        genresLabel.text = genres
        movieId = movie.id
        movieDescription.text = movie.overview
        releaseDateString = DateHelper.getFormattedDate(from: movie.release_date)
    }
    
    @objc func favButtonTapped(_ sender: UIButton) {
        guard let delegate = delegate else { return }
        guard let movieId = movieId else { return }
        if isFavoriteMovie {
            isFavoriteMovie = false
            delegate.removeFavorite(movieId)
        } else {
            isFavoriteMovie = true
            delegate.addFavorite(movieId)
        }
    }
}

extension DetailsScene: ViewCode {
    func createViewsHierarchy() {
        addSubview(moviePoster)
        addSubview(hStack)
        hStack.addArrangedSubview(releaseDateLabel)
        hStack.addArrangedSubview(favButton)
        addSubview(genresLabel)
        addSubview(movieDescription)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            moviePoster.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            moviePoster.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            moviePoster.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            moviePoster.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor, multiplier: 1/3),
            
            hStack.leadingAnchor.constraint(equalTo: moviePoster.leadingAnchor, constant: 8),
            hStack.topAnchor.constraint(equalTo: moviePoster.bottomAnchor, constant: 10),
            hStack.heightAnchor.constraint(equalToConstant: 25),
            hStack.widthAnchor.constraint(equalTo: moviePoster.widthAnchor, constant: -16),
            
            genresLabel.leadingAnchor.constraint(equalTo: hStack.leadingAnchor),
            genresLabel.topAnchor.constraint(equalTo: hStack.bottomAnchor, constant: 10),
            genresLabel.heightAnchor.constraint(equalToConstant: 25),
            genresLabel.widthAnchor.constraint(equalTo: moviePoster.widthAnchor, constant: -16),
            
            movieDescription.leadingAnchor.constraint(equalTo: releaseDateLabel.leadingAnchor),
            movieDescription.topAnchor.constraint(equalTo: genresLabel.bottomAnchor, constant: 20),
            movieDescription.widthAnchor.constraint(equalTo: moviePoster.widthAnchor, constant: -16),
            movieDescription.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
    }
    
    func setAdditionalConfigurations() {
        backgroundColor = .background
    }
}
