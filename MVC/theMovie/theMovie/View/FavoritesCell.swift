//
//  FavoritesCell.swift
//  theMovie
//
//  Created by Carlos Barcelos on 04/07/21.
//

import UIKit

class FavoritesCell: UITableViewCell {
    
    weak var delegate: MoviesListHelperDelegate?
    
    //MARK: Variables, constants
    let rootHStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.alignment = .center
        sv.spacing = 10
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let titleHStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.spacing = 2
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let vStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.alignment = .leading
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    private let moviePoster: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    private let movieTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 22)
        lbl.textColor = .white
        lbl.numberOfLines = 2
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let releaseDate: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.textAlignment = .right
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let movieDescription: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 0
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
   //MARK: Awake from Nib, Init
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "Favorite")
        setupView()
        self.isUserInteractionEnabled = true
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Custom functions
    
    func setupCell(for movie: Movie) {
        movieTitle.text = movie.title
        movieDescription.text = movie.overview
        
        if let posterPath = movie.poster_path {
            guard let url = Service.constructPosterUrl(from: posterPath) else { return }
            moviePoster.downloadImage(from: url)
        } else {
            moviePoster.image = UIImage(named: "poster_default")
        }
        
        if let date = movie.release_date {
            releaseDate.text = DateHelper.getFormattedDate(from: date)
        }
    }
}

//MARK: ViewCode configurations

extension FavoritesCell: ViewCode {
    func createViewsHierarchy() {
        addSubview(rootHStack)
        rootHStack.addArrangedSubview(moviePoster)
        rootHStack.addArrangedSubview(vStack)
        vStack.addArrangedSubview(titleHStack)
        titleHStack.addArrangedSubview(movieTitle)
        titleHStack.addArrangedSubview(releaseDate)
        vStack.addArrangedSubview(movieDescription)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            
            rootHStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            rootHStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            rootHStack.heightAnchor.constraint(equalToConstant: 190),
            rootHStack.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            
            moviePoster.heightAnchor.constraint(equalTo: rootHStack.heightAnchor),
            moviePoster.widthAnchor.constraint(equalTo: rootHStack.widthAnchor, multiplier: 0.35),
            
            vStack.heightAnchor.constraint(equalTo: rootHStack.heightAnchor),
            
            titleHStack.widthAnchor.constraint(equalTo: vStack.widthAnchor, multiplier: 0.95),
            titleHStack.heightAnchor.constraint(equalTo: vStack.heightAnchor, multiplier: 0.25),
            
            movieTitle.widthAnchor.constraint(equalTo: titleHStack.widthAnchor, multiplier: 0.65),
            
            movieDescription.widthAnchor.constraint(equalTo: titleHStack.widthAnchor, multiplier: 0.95),
        ])
    }
    
    func setAdditionalConfigurations() {
        backgroundColor = .background
        rootHStack.backgroundColor = .cell
    }
}
