//
//  ViewCode.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

protocol ViewCode {
    func createViewsHierarchy()
    func setupConstraints()
    func setAdditionalConfigurations()
    func setupView()
}

extension ViewCode {
    func setupView() {
        createViewsHierarchy()
        setupConstraints()
        setAdditionalConfigurations()
    }
}
