//
//  UIImageView+DownloadImage.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadImage(from url: URL) {
        let session = URLSession.shared.dataTask(with: url) { data, _, _ in
            if let data = data {
                let image = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.image = image
                }
            }
        }
        session.resume()
    }
}
