//
//  UIColor+ProjectColors.swift
//  theMovie
//
//  Created by Carlos Barcelos on 05/07/21.
//

import Foundation
import UIKit

extension UIColor {
    static var background: UIColor { return UIColor(red: 0.15, green: 0.20, blue: 0.22, alpha: 1.00) }
    static var cell: UIColor { return UIColor(red: 0.31, green: 0.36, blue: 0.38, alpha: 1.00) }
}
