//
//  Endpoint.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

protocol Endpoint {
    var host: String { get }
    var path: String { get }
    var method: String { get }
    var apiKey: [String:String] { get }
}
