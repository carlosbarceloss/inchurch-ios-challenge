//
//  Service.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

class Service {
    var isPaginating = false
    private var page = 1
    
    func request<T: Codable>(endpoint: Endpoint, pagination: Bool = false, completion: @escaping (Result<T, RequestError>) -> Void) {
        if pagination {
            isPaginating = true
        }

        if let urlRequest = makeUrlRequestFor(endpoint: endpoint, pagination: pagination) {
            
            let session = URLSession.shared.dataTask(with: urlRequest) { data, urlResponse, error in
                
                if let error = error {
                    completion(.failure(RequestError.networkError(error: error.localizedDescription)))
                }
                
                if let response = urlResponse as? HTTPURLResponse, let data = data {
                    if response.statusCode == 200 {
                            do {
                                completion(.success(try self.parse(data: data)))
                                if pagination {
                                    self.isPaginating = false
                                }
                            } catch {
                                completion(.failure(RequestError.parseError(error: error.localizedDescription)))
                            }
                    }
                    //Response != 200
                    completion(.failure(RequestError.backendError(statusCode: response.statusCode)))
                }
                //Não conseguiu obter dados
                completion(.failure(RequestError.generalError))
            }
            session.resume()
        }
        //Erro ao montar URL
        completion(.failure(RequestError.invalidURL))
    }
    
    private func makeUrlRequestFor(endpoint: Endpoint, pagination: Bool) -> URLRequest? {
        guard let url = urlConstructFrom(endpoint: endpoint, pagination: pagination) else { return nil }
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        urlRequest.httpMethod = endpoint.method
        return urlRequest
    }
    
    private func urlConstructFrom(endpoint: Endpoint, pagination: Bool) -> URL? {
        var component = URLComponents()
        component.scheme = "https"
        component.host = endpoint.host
        component.path = endpoint.path
        component.queryItems = endpoint.apiKey.map({ (key, value) in
            return URLQueryItem(name: key, value: value)
        })
        if pagination {
            page += 1
            component.queryItems?.append(URLQueryItem(name: "page", value: String(page)))
        }
        return component.url
    }
    
    private func parse<T: Decodable>(data: Data) throws -> T {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    
    static func constructPosterUrl(from posterId: String) -> URL? {
        let endpoint = ApiEndpoints.moviePoster(id: posterId)
        var component = URLComponents()
        component.scheme = "https"
        component.host = endpoint.host
        component.path = endpoint.path
        return component.url
    }
}
