//
//  ApiEndpoints.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

enum ApiEndpoints {
    case moviesList
    case genres
    case moviePoster(id: String)
}

extension ApiEndpoints: Endpoint {
    
    var host: String {
        switch self {
        case .moviePoster:
            return "image.tmdb.org"
        case .moviesList, .genres:
            return "api.themoviedb.org"
        }
    }
    
    var path: String {
        switch self {
        case .moviesList:
            return "/3/movie/popular"
        case .genres:
            return "/3/genre/movie/list"
        case .moviePoster(let id):
            return "/t/p/w500\(id)"
        }
    }
    
    var method: String {
        return "get"
    }
    
    var apiKey: [String:String] {
        return ["api_key":"ae601112952acaa10649fe7b5eea62c2"]
    }
}
