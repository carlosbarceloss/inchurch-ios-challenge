//
//  ApiError.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

enum RequestError: Error {
    case generalError
    case invalidURL
    case imageDownloadError
    case networkError(error: String)
    case backendError(statusCode: Int)
    case parseError(error: String)
}
