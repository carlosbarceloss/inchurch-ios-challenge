//
//  Genres.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

struct Genres: Codable {
    let genres: [Genre]
}

struct Genre: Codable {
    let id: Int
    let name: String
}
