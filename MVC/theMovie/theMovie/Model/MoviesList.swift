//
//  Movies.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

struct MoviesList: Codable {
    let results: [Movie]
}

