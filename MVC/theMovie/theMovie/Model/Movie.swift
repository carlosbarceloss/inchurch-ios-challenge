//
//  Movie.swift
//  theMovie
//
//  Created by Carlos Barcelos on 02/07/21.
//

import Foundation

struct Movie: Codable {
    let id: Int?
    let title: String?
    let poster_path: String?
    let backdrop_path: String?
    let overview: String?
    let release_date: String?
    let genre_ids: [Int]?
    var isFavorite: Bool? = false
}

extension Movie {
    mutating func toggleFavorite() {
        if isFavorite == nil {
            self.isFavorite = false
        } else {
            self.isFavorite?.toggle()
        }
    }
}

